from django.http.response import HttpResponse
from django.shortcuts import render

from django.http import HttpResponse


from .models import Room
# Create your views here.

# rooms = [
#     {'id': 1, 'name': 'Lets Learn Python'},
#     {'id': 2, 'name': 'Design with me'},
#     {'id': 3, 'name': 'Lets Learn Django'},
# ]



 
def home(request):

    # getting all rooms
    rooms = Room.objects.all()
    context = {'rooms':rooms }
    return render(request, 'base/home.html',context)


def room(request, pk):
 

    #getting single objects by its index
    # room = None
    # for i in rooms:
    #     if i['id'] == int(pk):
    #         room = i

    room = Room.objects.get(id=pk)
    context = {'room':room}
    return render(request, 'base/room.html')